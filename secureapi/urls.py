from django.conf.urls import url
import secureapi.views as views
urlpatterns = [
    url(r'^noauthuserlist/$', views.view, name='view'),
    url(r'^userlist/$', views.HelloView.as_view(), name='view'),
    url(r'^user_token/$', views.CustomAuthToken.as_view(), name='UserToken'),
    url(r'^taskb/$', views.TaskB.as_view(), name='TaskB'),
    ]