from rest_framework import serializers

from django.contrib.auth.models import Group, User
#from django_blog.apps.blog.models import Tag, Post


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'email', 'first_name', 'last_name',)