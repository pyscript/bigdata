from django.test import TestCase
from datetime import datetime

from django.shortcuts import render
from rest_framework.decorators import api_view, throttle_classes
from django.http import JsonResponse
from django.contrib.auth.models import Group, User
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
# Create your tests here.

num_tickets = int(input())
lst = []
for k in range(0, num_tickets):

    t1 = datetime.strptime(input(), '%a %d %b %Y %H:%M:%S %z')
    t2 = datetime.strptime(input(), '%a %d %b %Y %H:%M:%S %z')

    lst.append(abs(int((t1-t2).total_seconds())))

for item in lst:
    print(str(item))
