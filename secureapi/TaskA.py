from django.test import TestCase
from datetime import datetime
# Create your tests here.

num_tickets = int(input())
lst = []
for k in range(0, num_tickets):

    t1 = datetime.strptime(input(), '%a %d %b %Y %H:%M:%S %z')
    t2 = datetime.strptime(input(), '%a %d %b %Y %H:%M:%S %z')

    lst.append(abs(int((t1-t2).total_seconds())))

for item in lst:
    print(str(item))
