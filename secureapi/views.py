from django.shortcuts import render
from rest_framework.decorators import api_view, throttle_classes
from django.http import JsonResponse
from django.contrib.auth.models import Group, User
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
# Create your views here.

#https://simpleisbetterthancomplex.com/tutorial/2018/11/22/how-to-implement-token-authentication-using-django-rest-framework.html
#python manage.py drf_create_token shakil30629
#Generated token 5a3ab36d073b2ade2eba1376180a5db102ee5dbc for user shakil30629

class TaskB(APIView):
    def post(self, request, *args, **kwargs):
        from datetime import datetime
        #serializer = self.serializer_class(data=request.data, context={'request': request})
        #serializer.is_valid(raise_exception=True)


        timeset = str(request.data["Data"]).strip().split("\n")
        lst = []
        testCase=int(timeset[0])
        timeset.pop(0)

        for k in range(0,testCase):
            t1 = datetime.strptime(str(timeset[k * 2]), '%a %d %b %Y %H:%M:%S %z')
            t2 = datetime.strptime(str(timeset[k * 2 + 1]), '%a %d %b %Y %H:%M:%S %z')

            lst.append(str(abs(int((t1 - t2).total_seconds()))))


        #user = serializer.validated_data['user']
        #user = serializer.validated_data['user']
        #token, created = Token.objects.get_or_create(user=user)

        return JsonResponse(lst, safe=False)
        # return JsonResponse({
        #     'token': 'N/A',
        #     'user_id': 'N/A',
        #     'email': 'N/A'
        # })

@api_view(['GET'])
def view(request):
    #permission_classes = (IsAuthenticated,)
    user_list = list(User.objects.all().values('pk', 'email', 'first_name', 'last_name'))

    return JsonResponse(user_list,safe=False)

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        #print('----===--'+str(request.get('username'))+'==='+str(request.get('password')))
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return JsonResponse({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })

class HelloView(APIView):
    permission_classes = (IsAuthenticated,)             # <-- And here

    def get(self, request):
        user_list = list(User.objects.all().values('pk', 'email', 'first_name', 'last_name'))
        #content = {'message': 'Hello, World!'}
        return JsonResponse(user_list,safe=False)