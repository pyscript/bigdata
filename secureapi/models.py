from django.db import models
from django.db import models
from django.contrib.auth.models import Group, User
from django.contrib.auth.models import AbstractUser

class EtfPositionTitle(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A',unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Position'
        managed = False
        db_table = 'etf_position_title'

class EtfJobTitle(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A',unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Job Title'
        managed = False
        db_table = 'etf_job_title'

class EtfDivision(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A',unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Division'
        managed = False
        db_table = 'etf_division'

class EtfDepartment(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A',unique=True)
    DivisionId = models.ForeignKey(EtfDivision, db_column='DivisionId', on_delete=models.CASCADE, default=1,
                                   verbose_name='Division')

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Department'
        managed = False
        db_table = 'etf_department'

class EtfGrade(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A',unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Grade'
        managed = False
        db_table = 'etf_grade'

class EtfWorkLocation(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A',unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Working Location'
        managed = False
        db_table = 'etf_work_location'

class EtfIPs(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'IPs List'
        managed = False
        db_table = 'etf_ips'

class EtfUser(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    PIN = models.CharField(max_length=250, db_column='PIN', default='N/A', unique=True)
    LANId = models.CharField(max_length=250, db_column='LANId', default='N/A', verbose_name='LAN Id')
    Name = models.CharField(max_length=250, db_column='Name', default='N/A', verbose_name='Name')
    ManagerPIN = models.CharField(max_length=250, db_column='ManagerPIN', default='N/A', verbose_name='Manager PIN')

    PositionTitleId = models.ForeignKey(EtfPositionTitle, db_column='PositionTitleId', on_delete=models.CASCADE, default=1,verbose_name='Position')
    JobTitleId = models.ForeignKey(EtfJobTitle, db_column='JobTitleId', on_delete=models.CASCADE, default=1, verbose_name='Job Title')
    DivisionId = models.ForeignKey(EtfDivision, db_column='DivisionId', on_delete=models.CASCADE, default=1, verbose_name='Division')

    DepartmentId = models.ForeignKey(EtfDepartment, db_column='DepartmentId', on_delete=models.CASCADE, default=1, verbose_name='Department')
    #GradeId = models.ForeignKey(EtfGrade, db_column='GradeId', on_delete=models.CASCADE, default=1)
    WorkLocationId = models.ForeignKey(EtfWorkLocation, db_column='WorkLocationId', on_delete=models.CASCADE, default=1, verbose_name='Work Location')
    EtfIPs = models.ManyToManyField(EtfIPs, db_column='etf_ips_id', verbose_name='IPs')

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'User Info'
        managed = False
        db_table = 'etf_users'

# Create your models here.
class JobAssaigner(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Assaigner = models.ForeignKey(User, db_column='Assaigner', on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.Assaigner.name

    class Meta:
        managed = False
        db_table = 'jobassaigner'

class AccessMode(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    ModeName = models.CharField(max_length=200, db_column='ModeName', default='N/A')

    def __str__(self):
        return self.ModeName

    class Meta:
        managed = False
        db_table = 'access_mode'

class JobStatus(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=200, db_column='Name', default='N/A')

    def __str__(self):
        return self.Name

    class Meta:
        managed = False
        db_table = 'job_status'

class ApplicationType(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Application Type'
        managed = False
        db_table = 'application_type'

class ApplicationPriority(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Application Priority'
        managed = False
        db_table = 'application_priority'

class JobSubTypeName(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=200, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Job Sub Type Name List'
        managed = False
        db_table = 'job_sub_type_name'

class JobType(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    TypeName = models.CharField(max_length=200, db_column='TypeName', default='N/A',unique=True)
    IfHasAnyDetail = models.CharField(max_length=10, db_column='IfHasAnyDetail', default='N/A')

    def __str__(self):
        return self.TypeName

    class Meta:
        verbose_name_plural = 'Job Type'
        managed = False
        db_table = 'job_type'

class JobSubType(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    JobTypeId = models.ForeignKey(JobType, db_column='JobTypeId', on_delete=models.CASCADE, default=1, verbose_name='Job Type')
    JobSubTypeNameId = models.ForeignKey(JobSubTypeName, db_column='JobSubTypeNameId', on_delete=models.CASCADE, default=1, verbose_name='Job Sub Type')
    ApplicationTypeId = models.ForeignKey(ApplicationType, db_column='ApplicationTypeId', on_delete=models.CASCADE, default=1, verbose_name='Type')
    ApplicationPriorityId = models.ForeignKey(ApplicationPriority, db_column='ApplicationPriorityId', on_delete=models.CASCADE, default=1, verbose_name='Priority')
    SLATime = models.IntegerField(db_column='SLATime', default=0, verbose_name='SLA Time')

    def __str__(self):
        return self.JobSubTypeNameId.Name

    class Meta:
        unique_together = ('JobTypeId','JobSubTypeNameId', 'ApplicationTypeId', 'ApplicationPriorityId')
        verbose_name_plural = 'Job Sub Type'
        managed = False
        db_table = 'job_sub_type'

class AccessRequestMedia(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Access Request Media'
        managed = False
        db_table = 'access_request_media'

class Application(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)
    ApplicationTypeId = models.ForeignKey(ApplicationType, db_column='ApplicationTypeId', on_delete=models.CASCADE, default=1, verbose_name='Application Type')
    ApplicationPriorityId = models.ForeignKey(ApplicationPriority, db_column='ApplicationPriorityId', on_delete=models.CASCADE, default=1, verbose_name='Application Priority')

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Application List'
        managed = False
        db_table = 'application'

class JobMaster(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    MailTitle = models.TextField(db_column='MailTitle')
    RequestById = models.ForeignKey(EtfUser, db_column='RequestById', on_delete=models.CASCADE, default=1)
    AccessRequestMediaId = models.ForeignKey(AccessRequestMedia, db_column='AccessRequestMediaId', on_delete=models.CASCADE, default=1)

    ApplicationId = models.ForeignKey(Application, db_column='ApplicationId', on_delete=models.CASCADE, default=1)
    SLATime = models.IntegerField(db_column='SLATime', default=0)

    From = models.ForeignKey(User, related_name='JobMaster_From', on_delete=models.CASCADE, default=1)
    To = models.ForeignKey(User, related_name='JobMaster_To', on_delete=models.CASCADE, default=1)

    JobTypeId = models.ForeignKey(JobType, db_column='JobTypeId', on_delete=models.CASCADE, default=1)
    JobSubTypeId = models.ForeignKey(JobSubType, db_column='JobSubTypeId', on_delete=models.CASCADE, default=1)
    AccessModeId = models.ForeignKey(AccessMode, db_column='AccessModeId', on_delete=models.CASCADE, default=1)
    IsAccepet = models.CharField(max_length=10, db_column='IsAccepet', default='Y')
    JobStatusId = models.ForeignKey(JobStatus, db_column='JobStatusId', on_delete=models.CASCADE, default=1)

    IsTimeDurationHas = models.CharField(max_length=10, db_column='IsTimeDurationHas', default='N')
    AssaignerComment = models.TextField(db_column='AssaignerComment')
    FinisherComment = models.TextField(db_column='FinisherComment')

    Purpose = models.TextField(db_column='Purpose')
    Remarks = models.TextField(db_column='Remarks')

    MailReceivedDate = models.DateTimeField(db_column='MailReceivedDate')
    JobStartDate = models.DateTimeField(db_column='JobStartDate')
    JobEndDate = models.DateTimeField(db_column='JobEndDate')
    MailReceivedTime = models.CharField(max_length=20, db_column='MailReceivedTime', default='N/A')
    JobStartTime = models.CharField(max_length=20, db_column='JobStartTime', default='N/A')
    JobEndTime = models.CharField(max_length=20, db_column='JobEndTime', default='N/A')
    ApprovedTime = models.CharField(max_length=20, db_column='ApprovedTime', default='N/A')

    JobAccepetDate = models.DateTimeField(db_column='JobAccepetDate')

    AccessStartDate = models.DateTimeField(db_column='AccessStartDate')
    AccessEndDate = models.DateTimeField(db_column='AccessEndDate')
    ApprovedDate = models.DateTimeField(db_column='ApprovedDate')

    EntryDate = models.DateTimeField(auto_now_add=True, db_column='EntryDate')
    EditDate = models.DateTimeField( db_column='EditDate')

    def __str__(self):
        return self.RequestBy+'-'+self.PIN

    class Meta:
        managed = False
        db_table = 'job_master'

class JobDetail(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    JobMasterId = models.ForeignKey(JobMaster, db_column='JobMasterId', on_delete=models.CASCADE, default=1)
    BeneficiaryId = models.ForeignKey(EtfUser, db_column='BeneficiaryId', on_delete=models.CASCADE, default=1)
    LanIP = models.ForeignKey(EtfIPs, related_name='EtfIPs_LanIP', on_delete=models.CASCADE, default=1)
    WifyIP = models.ForeignKey(EtfIPs, related_name='EtfIPs_WifyIP', on_delete=models.CASCADE, default=1)
    DestinationIP = models.CharField(max_length=200, db_column='DestinationIP', default='N/A')
    Port = models.CharField(max_length=200, db_column='Port', default='N/A')

    def __str__(self):
        return self.BeneficiaryId.PIN

    class Meta:
        managed = False
        db_table = 'job_detail'

class MobileInfo(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    UserId = models.ForeignKey(User, db_column='UserId', on_delete=models.CASCADE, default=1)
    MobileNumber = models.CharField(max_length=200, db_column='MobileNumber', default='N/A')

    def __str__(self):
        return self.MobileNumber

    class Meta:
        managed = False
        db_table = 'mobile_info'

class EventLog(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    JobMasterId = models.ForeignKey(JobMaster, db_column='JobMasterId', on_delete=models.CASCADE, default=1)
    UserId = models.ForeignKey(User, db_column='UserId', on_delete=models.CASCADE, default=1)
    JobStatusId = models.ForeignKey(JobStatus, db_column='JobStatusId', on_delete=models.CASCADE, default=1)
    EntryDate = models.DateTimeField(auto_now_add=True, db_column='EntryDate')

    def __str__(self):
        return self.JobMasterId.Id

    class Meta:
        managed = False
        db_table = 'event_log'

class TaskCategory(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Task Category'
        managed = False
        db_table = 'task_category'

class TaskStatus(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Task Status'
        managed = False
        db_table = 'task_status'

class TaskType(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Task Type'
        managed = False
        db_table = 'task_type'

class TaskRegister(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.TextField(db_column='Name')
    TaskCategoryId = models.ForeignKey(TaskCategory, db_column='TaskCategoryId', on_delete=models.CASCADE, default=1, verbose_name='Task Category')
    TaskStatusId = models.ForeignKey(TaskStatus, db_column='TaskStatusId', on_delete=models.CASCADE, default=1, verbose_name='Task Status')
    TaskTypeId = models.ForeignKey(TaskType, db_column='TaskTypeId', on_delete=models.CASCADE, default=1, verbose_name='Task Type')
    UserId = models.ForeignKey(User, db_column='UserId', on_delete=models.CASCADE, default=1)
    EntryDate = models.DateTimeField(auto_now_add=True, db_column='EntryDate')

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Task Register'
        unique_together = (('Name', 'TaskCategoryId','UserId',),)
        managed = False
        db_table = 'task_register'

class TaskPriority(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Task Priority'
        managed = False
        db_table = 'task_priority'

class PlanStatus(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    Name = models.CharField(max_length=255, db_column='Name', default='N/A', unique=True)

    def __str__(self):
        return self.Name

    class Meta:
        verbose_name_plural = 'Plan Status'
        managed = False
        db_table = 'plan_status'

class PlanMaster(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')

    From = models.ForeignKey(User, related_name='PlanMaster_From', on_delete=models.CASCADE, default=1)
    To = models.ForeignKey(User, related_name='PlanMaster_To', on_delete=models.CASCADE, default=1)
    PlanCreator = models.CharField(max_length=10, db_column='PlanCreator', default='self', unique=True)
    PlanStatusId = models.ForeignKey(PlanStatus, db_column='PlanStatusId', on_delete=models.CASCADE, default=1)

    Comment = models.TextField(db_column='Comment')
    Remarks = models.TextField(db_column='Remarks')

    PlanSubmitDate = models.DateTimeField(db_column='PlanSubmitDate')
    PlanAccRevDate = models.DateTimeField(db_column='PlanAccRevDate')
    EntryDate = models.DateTimeField(db_column='EntryDate')
    EditDate = models.DateTimeField(db_column='EditDate')

    def __str__(self):
        return self.From.username

    class Meta:
        managed = False
        db_table = 'plan_master'


class PlanDetail(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')

    PlanMasterId = models.ForeignKey(PlanMaster, db_column='PlanMasterId', on_delete=models.CASCADE, default=1)
    TaskRegisterId = models.ForeignKey(TaskRegister, db_column='TaskRegisterId', on_delete=models.CASCADE, default=1)
    TaskStatusId = models.ForeignKey(TaskStatus, db_column='TaskStatusId', on_delete=models.CASCADE, default=1)
    TaskPriorityId = models.ForeignKey(TaskPriority, db_column='TaskPriorityId', on_delete=models.CASCADE, default=1)

    Comment = models.TextField(db_column='Comment')

    PlannedTaskStartTime = models.DateTimeField(db_column='PlannedTaskStartTime')
    PlanStartTime = models.CharField(max_length=255, db_column='PlanStartTime', default='N/A')
    PlannedDuration = models.IntegerField(db_column='PlannedDuration', default=0)
    PlannedTaskEndTime = models.DateTimeField(db_column='PlannedTaskEndTime')

    class Meta:
        managed = False
        db_table = 'plan_detail'

class PlanDetailLog(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')

    PlanMasterId = models.ForeignKey(PlanMaster, db_column='PlanMasterId', on_delete=models.CASCADE, default=1)
    TaskRegisterId = models.ForeignKey(TaskRegister, db_column='TaskRegisterId', on_delete=models.CASCADE, default=1)
    TaskStatusId = models.ForeignKey(TaskStatus, db_column='TaskStatusId', on_delete=models.CASCADE, default=1)
    TaskPriorityId = models.ForeignKey(TaskPriority, db_column='TaskPriorityId', on_delete=models.CASCADE, default=1)

    Comment = models.TextField(db_column='Comment')
    EntryDate = models.DateTimeField(db_column='EntryDate')

    PlannedTaskStartTime = models.DateTimeField(db_column='PlannedTaskStartTime')
    PlanStartTime = models.CharField(max_length=255, db_column='PlanStartTime', default='N/A')
    PlannedDuration = models.IntegerField(db_column='PlannedDuration', default=0)
    PlannedTaskEndTime = models.DateTimeField(db_column='PlannedTaskEndTime')

    class Meta:
        managed = False
        db_table = 'plan_detail_log'


class PlanExecutionDetail(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')

    PlanMasterId = models.ForeignKey(PlanMaster, db_column='PlanMasterId', on_delete=models.CASCADE, default=1)
    TaskRegisterId = models.ForeignKey(TaskRegister, db_column='TaskRegisterId', on_delete=models.CASCADE, default=1)
    TaskStatusId = models.ForeignKey(TaskStatus, db_column='TaskStatusId', on_delete=models.CASCADE, default=1)
    TaskPriorityId = models.ForeignKey(TaskPriority, db_column='TaskPriorityId', on_delete=models.CASCADE, default=1)

    Comment = models.TextField(db_column='Comment')

    ActualTaskStartTime = models.DateTimeField(db_column='ActualTaskStartTime')
    ActualStartTime = models.CharField(max_length=255, db_column='ActualStartTime', default='N/A')
    ActualDuration = models.IntegerField(db_column='ActualDuration', default=0)
    ActualTaskEndTime = models.DateTimeField(db_column='ActualTaskEndTime')

    class Meta:
        managed = False
        db_table = 'plan_execution_detail'


class LineManager(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')

    Manager = models.ForeignKey(User, related_name='LineManager_Manager', on_delete=models.CASCADE, default=1)
    Resource = models.ForeignKey(User, related_name='LineManager_Resource', on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.Manager.username

    class Meta:
        managed = False
        db_table = 'line_manager'

class HitLog(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id')
    UserId = models.ForeignKey(User, db_column='UserId', on_delete=models.CASCADE, default=1)
    IsLoggOut = models.CharField(max_length=10, db_column='IsLoggOut', default='N')
    LoggInTime = models.DateTimeField(auto_now_add=True, db_column='LoggInTime')
    LoggOutTime = models.DateTimeField(auto_now_add=True, db_column='LoggOutTime')

    def __str__(self):
        return self.UserId.id

    class Meta:
        managed = False
        db_table = 'hit_log'
