from django.apps import AppConfig


class SecureapiConfig(AppConfig):
    name = 'secureapi'
